package com.jakes.cointracker;

import android.graphics.Color;
import android.os.AsyncTask;
import android.view.View;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.Line;
import com.echo.holographlibrary.LineGraph;
import com.echo.holographlibrary.LinePoint;
import com.echo.holographlibrary.PieGraph;
import com.echo.holographlibrary.PieSlice;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jake on 6/30/2014.
 */
public class GenerateGraphTask extends AsyncTask<Void, Void, Void> {
    View rootView;
    JSONArray trades;
    JSONObject market;

    public GenerateGraphTask(View view, JSONArray trades, JSONObject market){
        this.rootView = view;
        this.trades = trades;
        this.market = market;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            Line l = new Line();
            l.setColor(Color.parseColor("#ffeb3b"));
            l.setShowingPoints(true);
            l.setUsingDips(true);
            l.setStrokeWidth(1);
            int buys = 0;
            int sells = 0;
            for (int i = 0; i < trades.length(); i++) {
                JSONObject obj = (JSONObject) trades.get(i);
                double price = obj.getDouble("price");
                int type = obj.getInt("type");
                if (type == 1) {
                    buys++;
                } else {
                    sells++;
                }
                if (i % 5 == 0) {
                    LinePoint lp = new LinePoint();
                    lp.setSelectedColor(Color.parseColor("#ff5722"));
                    lp.setColor(Color.parseColor("#33b5e5"));
                    lp.setX(i);
                    lp.setY((float) price);
                    l.addPoint(lp);
                }
            }

            LineGraph li = (LineGraph) rootView.findViewById(R.id.line_graph);
            if (l.getPoints().size() > 0)
                li.addLine(l);
            li.animate();
            li.setRangeX(0, 90);
            li.setRangeY((float) market.getDouble("24hlow"), (float) market.getDouble("24hhigh") + ((float) (market.getDouble("24hhigh"))) * 0.25f);
            li.setLineToFill(0);

            PieGraph pg = (PieGraph) rootView.findViewById(R.id.pie_graph);
            PieSlice slice = new PieSlice();
            slice.setColor(Color.parseColor("#259b24"));
            slice.setValue(buys);
            slice.setTitle("Bids");
            pg.addSlice(slice);
            slice = new PieSlice();
            slice.setColor(Color.parseColor("#e51c23"));
            slice.setTitle("Asks");
            slice.setValue(sells);
            pg.addSlice(slice);

            ArrayList<Bar> points = new ArrayList<Bar>();

            Bar d = new Bar();
            d.setValueColor(Color.parseColor("#222222"));
            d.setColor(Color.parseColor("#e51c23"));
            d.setName("Low");
            d.setValue((float) market.getDouble("24hlow"));
            Bar d2 = new Bar();
            d2.setValueColor(Color.parseColor("#222222"));
            d2.setColor(Color.parseColor("#259b24"));
            d2.setName("High");
            d2.setValue((float) market.getDouble("24hhigh"));
            Bar d3 = new Bar();
            d3.setValueColor(Color.parseColor("#222222"));
            d3.setColor(Color.parseColor("#8ddf79"));
            d3.setName("Last");
            d3.setValue((float) market.getDouble("last_price"));
            points.add(d);
            points.add(d2);
            points.add(d3);

            BarGraph g = (BarGraph) rootView.findViewById(R.id.bar_graph);
            g.setBars(points);
        } catch (Exception e){

        }
        return null;
    }
}
