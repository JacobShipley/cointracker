package com.jakes.cointracker;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.echo.holographlibrary.Line;

import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public class MainActivity extends Activity {

    public enum SortMode{
        CHANGE, VOLUME, NAME
    }

    public SortMode sortMode = SortMode.VOLUME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setExitTransition(new Explode());
        getWindow().setEnterTransition(new Explode());
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //if (id == R.id.action_settings) {
         //   return true;
        //}
        CharSequence msg;
        if(id == R.id.menu_sort_change){
            switch(sortMode){
                case VOLUME:
                    sortMode = SortMode.NAME;
                    msg = "" + getString(R.string.sort_name);
                    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                    break;
                case NAME:
                    sortMode = SortMode.CHANGE;
                    msg = "" + getString(R.string.sort_change);
                    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                    break;
                case CHANGE:
                    sortMode = SortMode.VOLUME;
                    msg = "" + getString(R.string.sort_volume);
                    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                    break;
            }
            PlaceholderFragment fragment = (PlaceholderFragment) getFragmentManager().findFragmentById(R.id.container);
            fragment.sortView(sortMode);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        ListView listView;
        ArrayList<Map<String, ?>> data;
        ArrayList<Map<String, ?>> data_alpha;
        JSONArray jArray;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            listView = (ListView) rootView.findViewById(R.id.list_market);

             data = new ArrayList<Map<String, ?>>();


            // ListView Item Click Listener
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Intent detailIntent = new Intent(view.getContext(), MarketActivity.class);
                    Map map = data.get(position);
                    int pos = Integer.parseInt((String)map.get("market"));
                    detailIntent.putExtra("market", pos);
                    detailIntent.putExtra("jArray", jArray.toString());
                    //detailIntent.putExtra(MarketDetailFragment.ARG_ITEM_ID, id);
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), view.findViewById(R.id.list_item), "summary");
                    try{
                        startActivity(detailIntent, options.toBundle());
                    } catch (Exception e){
                        Log.v("ERROR", e.getMessage());
                    }
                }
            });

            HttpGet marketSummary = new HttpGet("https://api.mintpal.com/market/summary/");
            Line l = new Line();

            try {
                jArray = new MintPalCall(rootView.getContext()).execute(marketSummary).get();

                for(int i = 0; i < jArray.length(); i++){
                    JSONObject jObject = jArray.getJSONObject(i);
                    // Pulling items from the array
                    String marketName = jObject.getString("code") + "/" + jObject.getString("exchange");
                    String coinName = jObject.getString("coin");
                    String marketChange = jObject.getString("change");
                    String marketHigh = jObject.getString("24hhigh");
                    String marketLast = jObject.getString("last_price");
                    String marketVolume = jObject.getString("24hvol");
                    Map map = new HashMap();
                    map.put("list_item", marketName);
                    map.put("list_coin_name", coinName);
                    map.put("list_item_change", marketChange);
                    map.put("list_item_high", marketHigh);
                    map.put("list_item_last", marketLast);
                    map.put("list_item_volume", marketVolume);
                    map.put("market", "" + i);
                    data.add(map);
                }
                data_alpha = data;
                //SimpleAdapter adapter = new SimpleAdapter(rootView.getContext(), data, R.layout.list_entry, new String[] {"list_item", "list_item_change"}, new int[] { R.id.list_item, R.id.list_item_change });
                MarketAdapter adapter = new MarketAdapter(rootView.getContext(), data);
                /* Assign adapter to ListView */
                listView.setAdapter(adapter);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return rootView;
        }

        public void sortView(SortMode mode){
            View rootView = getView();
            listView = (ListView) rootView.findViewById(R.id.list_market);
            switch(mode){
                case VOLUME:
                    Collections.sort(data, new VolumeSorter());
                    break;
                case CHANGE:
                    Collections.sort(data, new ChangeSorter());
                    break;
                case NAME:
                    data = new ArrayList<Map<String,?>>(data_alpha);
                    break;
            }
            //SimpleAdapter adapter = new SimpleAdapter(rootView.getContext(), data, R.layout.list_entry, new String[] {"list_item", "list_item_change"}, new int[] { R.id.list_item, R.id.list_item_change });
            MarketAdapter adapter = new MarketAdapter(rootView.getContext(), data);
                /* Assign adapter to ListView */
            listView.setAdapter(adapter);
        }

        public class VolumeSorter implements Comparator<Map<String,?>> {
            @Override
            public int compare(Map<String,?> o1, Map<String,?> o2) {
                if((Float.parseFloat((String)o1.get("list_item_volume"))) > (Float.parseFloat((String)o2.get("list_item_volume")))){
                    return -1;
                } else{
                    return 1;
                }
            }
        }

        public class ChangeSorter implements Comparator<Map<String,?>> {
            @Override
            public int compare(Map<String,?> o1, Map<String,?> o2) {
                if((Float.parseFloat((String)o1.get("list_item_change"))) > (Float.parseFloat((String)o2.get("list_item_change")))){
                    return -1;
                } else{
                    return 1;
                }
            }
        }
    }
}
