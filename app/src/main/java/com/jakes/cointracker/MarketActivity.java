package com.jakes.cointracker;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.echo.holographlibrary.LineGraph;
import com.jakes.cointracker.ui.TransitionAdapter;

import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class MarketActivity extends Activity {
    int marketNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setExitTransition(new Explode());
        getWindow().setEnterTransition(new Explode());
        setContentView(R.layout.activity_market);
        PlaceholderFragment fragment = new PlaceholderFragment();
        fragment.setArguments(getIntent().getExtras());
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }

        getWindow().getEnterTransition().addListener(new TransitionAdapter() {
            @Override
            public void onTransitionEnd(Transition transition) {
                TextView rootView = (TextView) findViewById(R.id.list_item);
                getWindow().getEnterTransition().removeListener(this);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.market, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //if (id == R.id.action_settings) {
        //    return true;
        //}
        finish();
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment{
        LineGraph li;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            int marketId = getArguments().getInt("market");
            View rootView = inflater.inflate(R.layout.fragment_market, container, false);

            JSONObject market = null;
            int marketNumber = getActivity().getIntent().getIntExtra("market", 0);
            CharSequence title = "";

            try {
                String jsonArray = getArguments().getString("jArray");
                JSONArray jArray = new JSONArray(jsonArray);
                market = jArray.getJSONObject(marketNumber);

                // Pulling items from the array
                String marketName = "";
                try {

                    marketName = market.getString("code") + "/" + market.getString("exchange");
                    title = marketName + " Details";
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                HttpGet tradeHistory = new HttpGet("https://api.mintpal.com/market/trades/" + marketName);
                JSONObject result = new MintPalObject(rootView.getContext()).execute(tradeHistory).get();
                JSONArray trades = result.getJSONArray("trades");

                new GenerateGraphTask(rootView, trades, market).execute();

                /*Line l = new Line();
                l.setColor(Color.parseColor("#ffeb3b"));
                l.setShowingPoints(true);
                l.setUsingDips(true);
                l.setStrokeWidth(1);
                int buys = 0;
                int sells = 0;
                for(int i = 0; i < trades.length(); i++){
                    JSONObject obj = (JSONObject) trades.get(i);
                    double price = obj.getDouble("price");
                    int type = obj.getInt("type");
                    if(type == 1){
                        buys++;
                    } else{
                        sells++;
                    }
                    if(i % 5 == 0){
                        LinePoint lp = new LinePoint();
                        lp.setSelectedColor(Color.parseColor("#ff5722"));
                        lp.setColor(Color.parseColor("#33b5e5"));
                        lp.setX(i);
                        lp.setY((float)price);
                        l.addPoint(lp);
                    }
                }

                li = (LineGraph) rootView.findViewById(R.id.line_graph);
                if(l.getPoints().size() > 0)
                    li.addLine(l);
                li.animate();
                li.setRangeX(0, 90);
                li.setRangeY((float) market.getDouble("24hlow"), (float) market.getDouble("24hhigh") + ((float) (market.getDouble("24hhigh")))*0.25f);
                li.setLineToFill(0);

                PieGraph pg = (PieGraph) rootView.findViewById(R.id.pie_graph);
                PieSlice slice = new PieSlice();
                slice.setColor(Color.parseColor("#259b24"));
                slice.setValue(buys);
                slice.setTitle("Bids");
                pg.addSlice(slice);
                slice = new PieSlice();
                slice.setColor(Color.parseColor("#e51c23"));
                slice.setTitle("Asks");
                slice.setValue(sells);
                pg.addSlice(slice);

                ArrayList<Bar> points = new ArrayList<Bar>();

                Bar d = new Bar();
                d.setValueColor(Color.parseColor("#222222"));
                d.setColor(Color.parseColor("#e51c23"));
                d.setName("Low");
                d.setValue((float) market.getDouble("24hlow"));
                Bar d2 = new Bar();
                d2.setValueColor(Color.parseColor("#222222"));
                d2.setColor(Color.parseColor("#259b24"));
                d2.setName("High");
                d2.setValue((float) market.getDouble("24hhigh"));
                Bar d3 = new Bar();
                d3.setValueColor(Color.parseColor("#222222"));
                d3.setColor(Color.parseColor("#8ddf79"));
                d3.setName("Last");
                d3.setValue((float) market.getDouble("last_price"));
                points.add(d);
                points.add(d2);
                points.add(d3);

                BarGraph g = (BarGraph) rootView.findViewById(R.id.bar_graph);
                g.setBars(points);*/
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // Show the Up button in the action bar.
            getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
            getActivity().getActionBar().setTitle(title);
            return rootView;
        }
    }
}
