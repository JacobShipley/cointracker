package com.jakes.cointracker;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Jake on 6/27/2014.
 */
public class MarketAdapter extends BaseAdapter {
    private ArrayList<Map<String,?>> data;
    private static LayoutInflater inflater;

    public MarketAdapter(Context ctx, ArrayList<Map<String,?>> data){
        this.data = data;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return -1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = inflater.inflate(R.layout.list_entry, null);
        }
        final View _view = view;
        Map<String, ?> market = data.get(i);

        TextView marketName = (TextView) view.findViewById(R.id.list_item);
        marketName.setText((String)market.get("list_item"));
        TextView coinName = (TextView) view.findViewById(R.id.list_coin_name);
        coinName.setText((String)market.get("list_coin_name"));
        TextView marketChange = (TextView) view.findViewById(R.id.list_item_change);
        marketChange.setText((String)market.get("list_item_change"));
        if(Float.parseFloat((String)market.get("list_item_change")) > 0.01){
            marketChange.setTextColor(Color.parseColor("#99cc00"));
        } else if (Float.parseFloat((String)market.get("list_item_change")) < -0.01){
            marketChange.setTextColor(Color.parseColor("#ff4444"));
        } else{
            marketChange.setTextColor(Color.parseColor("#222222"));
        }
        TextView marketHigh = (TextView) view.findViewById(R.id.list_item_high);
        marketHigh.setText((String)market.get("list_item_volume"));
        TextView marketLast = (TextView) view.findViewById(R.id.list_item_last);
        marketLast.setText((String)market.get("list_item_last"));
        return view;
    }
}
