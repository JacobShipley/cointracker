package com.jakes.cointracker;

/**
 * Created by Jake on 6/27/2014.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

public class MintPalCall extends AsyncTask<HttpGet, Integer, JSONArray>{
    Context ctx;

    public MintPalCall(Context ctx){
        this.ctx = ctx;
    }
    @Override
    protected JSONArray doInBackground(HttpGet... get) {
        HttpClient http = new DefaultHttpClient();
        JSONArray jArray = null;

        for(int i = 0; i < get.length; i++){
            // Add your data
            //List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            //get[i].setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            get[i].setHeader("Content-type", "application/json");
            try {
                HttpResponse response = http.execute(get[i]);
                String responseStr = EntityUtils.toString(response.getEntity());
                jArray = new JSONArray(responseStr);
				/*for (int j=0; j < jArray.length(); j++)
				{
				    try {
				        JSONObject oneObject = jArray.getJSONObject(j);
				        // Pulling items from the array
				        System.out.println(oneObject);
				       // String oneObjectsItem = oneObject.getString("STRINGNAMEinTHEarray");
				        //String oneObjectsItem2 = oneObject.getString("anotherSTRINGNAMEINtheARRAY");
				    } catch (JSONException e) {
				        // Oops
				    }
				}*/
            } catch (Exception e) {
                // Oops
                Log.v("Hello", e.getMessage());
            }
        }

        return jArray;
    }

    protected void onPostExecute(JSONArray result) {


    }
}