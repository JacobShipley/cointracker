package com.jakes.cointracker;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Jake on 6/27/2014.
 */
public class MintPalObject extends AsyncTask<HttpGet, Integer, JSONObject> {
    Context ctx;

    public MintPalObject(Context ctx){
        this.ctx = ctx;
    }
    @Override
    protected JSONObject doInBackground(HttpGet... get) {
        HttpClient http = new DefaultHttpClient();
        JSONObject jObject = null;

        for(int i = 0; i < get.length; i++){
            // Add your data
            //List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            //get[i].setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            get[i].setHeader("Content-type", "application/json");
            try {
                HttpResponse response = http.execute(get[i]);
                String responseStr = EntityUtils.toString(response.getEntity());
                jObject = new JSONObject(responseStr);
				/*for (int j=0; j < jArray.length(); j++)
				{
				    try {
				        JSONObject oneObject = jArray.getJSONObject(j);
				        // Pulling items from the array
				        System.out.println(oneObject);
				       // String oneObjectsItem = oneObject.getString("STRINGNAMEinTHEarray");
				        //String oneObjectsItem2 = oneObject.getString("anotherSTRINGNAMEINtheARRAY");
				    } catch (JSONException e) {
				        // Oops
				    }
				}*/
            } catch (Exception e) {
                // Oops
            }
        }

        return jObject;
    }

    protected void onPostExecute(JSONArray result) {


    }
}